import { youtubeAccelerated } from './youtube-accelerated/controller';

(function () {
    function isPlayerLaded() {
        const isVideoTagLoaded = document.getElementsByTagName('video').length > 0;
        const isControlsLoaded = document.getElementsByClassName('ytp-right-controls').length > 0;
        return isVideoTagLoaded && isControlsLoaded;
    }
    function lookForPlayer() {
        let timeout = 100;
        function checkForVideo() {
            if (isPlayerLaded()) {
                if ( document.getElementById('youtube-accelerated-playbackRate') ) {
                    return;
                }
                youtubeAccelerated();
                document.removeEventListener("click", checkForHrefChange, false);
            } else {
                timeout *= 1.5;
                if (timeout < 5000) setTimeout(checkForVideo, timeout);
                else console.log('Youtube Accelerated failed to add UI');
            }
        };
        setTimeout(checkForVideo, timeout);
    }
    let currentUrl = window.location.href;
    function checkForHrefChange() {
        setTimeout( _ => {
            console.log('click')
            if (currentUrl !== window.location.href) {
                lookForPlayer();
                currentUrl = window.location.href;
            }
        })
    }

    document.addEventListener("click", checkForHrefChange, false);
    lookForPlayer();
})();
