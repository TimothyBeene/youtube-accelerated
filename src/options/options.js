import { GlobalSpeed } from '../shared/storage';

function setInputValue(input, value) {
    if (typeof value === 'boolean') {
        const mdlCheckbox = input.parentElement.MaterialSwitch
        if (value) {
            mdlCheckbox.on()
        } else {
            mdlCheckbox.off()
        }
    } else {
        if (input.attributes['type'].value === 'range') input.MaterialSlider && input.MaterialSlider.change(value);
        else input.value = value;
    }
}
const storage = GlobalSpeed();
storage.onChange().subscribe( storage => {
    const showStoreView = storage.settings['show-store-view'];
    document.querySelector('#store-section')
        .style.display = showStoreView ? 'block' : 'none'
    if (showStoreView) {
        document.querySelector('#storage-view')
            .innerHTML = JSON.stringify(storage, null, 2);
    }
    Object.keys(storage.settings).forEach( key => {
        document.querySelectorAll(`div.option input[id='${key}']`)
            .forEach( input => setInputValue(input, storage.settings[key]))
    })
});

document.querySelectorAll('div.option input[type="number"]').forEach( el => {
    el.addEventListener( 'change', (event) => {
            const { name, value } = event.target;
            storage.get().then( store => {
                storage.set({
                    ...store,
                    settings: {
                        ...store.settings,
                        [name]: +value
                    }
                })
            })
        }
    )
});
document.querySelectorAll('div.option input[type="range"]').forEach( el => {
    el.addEventListener( 'input', (event) => {
            const { name, value } = event.target;
            storage.get().then( store => {
                storage.set({
                    ...store,
                    settings: {
                        ...store.settings,
                        [name]: +value
                    }
                })
            })
        }
    )
});
document.querySelectorAll('div.option input[type="checkbox"]').forEach( el => {
    el.addEventListener( 'change', (event) => {
            const { name, checked } = event.target;
            storage.get().then( store => {
                storage.set({
                    ...store,
                    settings: {
                        ...store.settings,
                        [name]: checked
                    }
                })
            })
        }
    )
});