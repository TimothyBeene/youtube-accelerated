export function YoutubeAcceleratedView(speedUp, slowDown, resetSpeed) {


    function createSlowDownButton(callback) {
        const slowDownButton = document.createElement('button');
        slowDownButton.setAttribute('id', 'youtube-accelerated-slowdown');
        slowDownButton.setAttribute('class', 'ytp-button');
        slowDownButton.innerHTML = '<svg viewBox="-6 -6 36 36" height="100%" width="100%"><path fill="#FFFFFF" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M7,13H17V11H7"/></svg>';
        slowDownButton.addEventListener('click', callback);
        return slowDownButton;
    }
    function createSpeedUpButton(callback) {
        const speedUpButton = document.createElement('button');
        speedUpButton.setAttribute('id', 'youtube-accelerated-speedup');
        speedUpButton.setAttribute('class', 'ytp-button');
        speedUpButton.innerHTML = '<svg viewBox="-6 -6 36 36" height="100%" width="100%"><path fill="#FFFFFF" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z"/></svg>';
        speedUpButton.addEventListener('click', callback);
        return speedUpButton;
    }
    function createPlaybackView(callback) {
        const playbackRateSpan = document.createElement('span');
        playbackRateSpan.addEventListener('click', callback);
        playbackRateSpan.style.cursor = 'pointer';

        const playbackRateDiv = document.createElement('div');
        playbackRateDiv.setAttribute('id', 'youtube-accelerated-playbackRate');
        playbackRateDiv.setAttribute('class', 'ytp-time-display');
        playbackRateDiv.appendChild(playbackRateSpan);

        return { playbackRateDiv, playbackRateSpan}
    }
    function createWpmView() {
        const wpmDiv = document.createElement('div');
        wpmDiv.setAttribute('id', 'youtube-accelerated-wpm');
        wpmDiv.setAttribute('class', 'ytp-time-display');
        const wpmSpan = document.createElement('span');
        wpmDiv.appendChild(wpmSpan);
        return { wpmDiv, wpmSpan };
    }
    const slowDownButton = createSlowDownButton(slowDown);
    const speedUpButton = createSpeedUpButton(speedUp);
    const { playbackRateDiv, playbackRateSpan } = createPlaybackView(resetSpeed);
    const { wpmDiv, wpmSpan } = createWpmView();
    
    const leftControls = document.getElementsByClassName('ytp-left-controls')[0];
    leftControls.appendChild(wpmDiv);

    const rightControls = document.getElementsByClassName('ytp-right-controls')[0];
    rightControls.prepend(slowDownButton, playbackRateDiv, speedUpButton);

    function setWpm(wpm) {
        if (wpm) {
            wpmSpan.textContent = ` @ ${(0.00 + wpm).toFixed(0)} wpm`;
        } else {
            wpmSpan.textContent = '';
        }
    }
    function setSpeed(speed) {
        playbackRateSpan.textContent = (0.00 + speed).toFixed(2);
    }

    return {
        setWpm,
        setSpeed
    };
}