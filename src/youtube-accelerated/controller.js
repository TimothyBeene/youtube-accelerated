import { GlobalSpeed } from '../shared/storage';
import { SpeechRate } from '../shared/speechRate';
import { Category } from '../shared/category';
import { Config } from '../shared/config';
import { YoutubeAcceleratedView } from './view';

import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

export function youtubeAccelerated() {
    const globalSpeed = GlobalSpeed();
    const speechRate = SpeechRate();
    const category = Category();
    const videoTag = document.getElementsByTagName('video')[0];
    let globalCatagoryId;
    let configStore = {};
    let videoSpeed = 1.0;

    const videoId$ = listenForVideoIdChange();

    videoId$.subscribe( ({videoId, catagoryId}) => {
        console.log(`videoId: ${videoId}, category: ${catagoryId}`);
        globalCatagoryId = catagoryId;
        globalSpeed.forceEventPush();
    })

    function setLocalVideoSpeed(speed) {
        if (!configStore['disable-memory']) return;

        videoSpeed = speed;
        videoTag.playbackRate = videoSpeed;
        view.setSpeed(videoSpeed);
    }

    function speedUp() {
        let timeOfLastSet = Date.now();
        return function() {
            globalSpeed.getSpeed(globalCatagoryId).then( speed => {
                let increment;
                if (Date.now() - timeOfLastSet < Config.threshold) {
                    increment = +configStore['large-increment'] || Config.largeIncriment;
                } else {
                    increment = +configStore['small-increment'] || Config.smallIncriment;
                }
                if (configStore['disable-memory']) {
                    setLocalVideoSpeed(videoSpeed + increment);
                } else {
                    globalSpeed.setSpeed(speed + increment, globalCatagoryId).then();
                }
                timeOfLastSet = Date.now();
            });
        };
    }
    function slowDown() {
        let timeOfLastSet = Date.now();
        return function() {
            let increment;
            globalSpeed.getSpeed(globalCatagoryId).then( speed => {
                if (Date.now() - timeOfLastSet < Config.threshold) {
                    increment = +configStore['large-increment'] || Config.largeIncriment;
                } else {
                    increment = +configStore['small-increment'] || Config.smallIncriment;
                }
                if (speed - increment <= 0.0) return;

                if (configStore['disable-memory']) {
                    setLocalVideoSpeed(videoSpeed - increment);
                } else {
                    globalSpeed.setSpeed(speed - increment, globalCatagoryId).then();
                }
                timeOfLastSet = Date.now();
            });
        };
    }
    function resetSpeed() {
        globalSpeed.setSpeed(1.0, globalCatagoryId);
    }
    const view = YoutubeAcceleratedView(speedUp(), slowDown(), resetSpeed );

    function mapCatagorySpeed(speed) {
        if (speed.catagories[globalCatagoryId]) {
            return +speed.catagories[globalCatagoryId];
        } else {
            return +speed.baseSpeed;
        }
    };
    globalSpeed.onChange().pipe(
            tap( store => configStore = store.settings ),
            map( store => store.speedStore ),
            map( speedStore => mapCatagorySpeed(speedStore) )
        ).subscribe( speed => {
            videoTag.playbackRate = speed;
            view.setSpeed(speed);
    });

    function correctPlaybackRate(event) {
        globalSpeed.getSpeed(globalCatagoryId).then( speed => {
            if (event.target.playbackRate !== speed) {
                if (configStore['disable-memory']) view.setSpeed(videoSpeed);
                else view.setSpeed(speed);
            }
        });
    };
    videoTag.addEventListener('ratechange', correctPlaybackRate);
    videoTag.addEventListener('playing', correctPlaybackRate);

    videoTag.addEventListener('timeupdate', () => {
        let wpm = speechRate.getWpm(videoTag.currentTime);
        if (wpm && !configStore['disable-wpm-calc']) {
            globalSpeed.getSpeed(globalCatagoryId).then(speed => {
                wpm *= speed;
                view.setWpm(wpm);
            });
        } else {
            view.setWpm();
        }
    });

    function listenForVideoIdChange() {
        return new Observable( observer => {
            function publishVideoId(videoId) {
                if (!videoId) {
                    videoId = document.getElementsByTagName('ytd-watch-flexy')[0].attributes['video-id'].value;
                }
                setLocalVideoSpeed(1.0);
                speechRate.loadCC(videoId)
                    .catch(e => {
                        console.log('captions failed to load', videoId, e);
                    })
                    .then();
                category.getCatagory(videoId)
                    .catch(() => observer.next( {videoId: videoId, catagoryId: null} ))
                    .then( cat => observer.next( {videoId: videoId, catagoryId: cat} ));
            };

            const mutationObserver = new MutationObserver(mutationsList => {
                function isVideoIdAttribute(mutation) {
                    return mutation.type === 'attributes' && mutation.attributeName === 'video-id';
                }
                if (mutationsList.find(isVideoIdAttribute)) {
                    publishVideoId();
                }
            });
            
            const ytWatchFlexy = document.getElementsByTagName('ytd-watch-flexy')[0];
            mutationObserver.observe(ytWatchFlexy, { attributes: true });

            // get videoId by parsing the uri
            const videoId = window.location.href.split('?')[1].split('&').find(i => i.split('=')[0] === 'v').split('=')[1];
            console.log('videoId from location href', videoId);
            publishVideoId(videoId);
        });
    };
}
