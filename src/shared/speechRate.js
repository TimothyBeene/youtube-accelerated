'use strict';
// var fetch = require('node-fetch');
// var fetch = require('./mock-cc');
// const DOMParser = require('xmldom').DOMParser;


export function SpeechRate() {
    let wpmList = [];
    function buildCaptionList(node) {
        const start = +node.attributes.start.value;
        const durr = +node.attributes.dur.value;
        return {
            start: start,
            durr: durr,
            end: start + durr,
            words: +removeXmlMarkup(node.textContent).split(/[ \n]/).length
        };
    }
    function removeXmlMarkup(text) {
        const clean = text.replace(/(<[^>]*>)/g, '');
        return clean;
    }
    function generateWpmList(xml) {
        const parser = new DOMParser();
        const doc = parser.parseFromString(xml, "application/xml");
        let textNodes = Array.from(doc.getElementsByTagName('text'));
        textNodes = textNodes
            .map(buildCaptionList);
        return textNodes;
    }
    function storeWpmList(list) {
        wpmList = list;
        return list;
    }

    function parseWatchPage(rawHtml) {
        return new Promise( (resolve, reject) => {
            // console.log('parseWatchPage rawHtml', rawHtml)
            const result = /ytInitialPlayerResponse"] = \(.*[\s\S]?(.*)\);/i.exec(rawHtml)[1];
            // console.log('parseWatchPage parsed', result);
            try {
                const playerResponse = JSON.parse(result);
                resolve(playerResponse.captions.playerCaptionsTracklistRenderer.captionTracks[0].baseUrl);
            } catch (error) {
                // console.log('parseWatchPage error', error)
                reject(error);
            }
        });
    }

    function getTimedTextUrl(videoId) {
        const baserUrl = 'https://www.youtube.com/watch';
        const url = new URL(baserUrl);
        url.searchParams.append('v', videoId);
        return fetch(url)
            .then( res => res.text() )
            .then(parseWatchPage)
            // .catch( error => console.log('getTimedTextUrl error', error) )
    }
    function loadCC(videoId) {
        return new Promise( (resolve, reject) => {
            getTimedTextUrl(videoId)
                .catch(e => {
                    storeWpmList([]);
                    return reject(e);
                })
                .then( baseUrl => {
                    // console.log('loadCC baseUrl', baseUrl)
                    const url = new URL(baseUrl);
                    fetch(url)
                        .catch(e => {
                            storeWpmList([]);
                            return reject(e);
                        })
                        .then(res => res.text())
                        .then(generateWpmList)
                        .then(storeWpmList.bind(this))
                        .then(resolve);
                });
        });
    }
    function clearCaptions(e) {
        storeWpmList([]);
        return e;

    }
    function nodeFallsInRange(node, from ,to) {
        // from falls in node
        if (node.start < from && from < node.end) return true;
        // to falls in node
        if (node.start < to && to < node.end) return true;
        // node falls in between from and to
        if (from < node.start && node.end < to) return true;
        return false;
    }
    function getWordsInSpan(wpmList, from, to) {
        const nodesInRange = wpmList.filter( val => nodeFallsInRange(val, from, to));
        if (nodesInRange.length < 1) return 0;
        return nodesInRange.reduce( (acc, curr) => acc + curr.words, 0 );
    }
    function getWpm(time, span = 60) {
        try {
            const words = getWordsInSpan(wpmList, time - span/2, time + span/2);
            return wpmList && words;
        } catch(e) {console.log(e)}
    }
    function hasWpm() {
        return wpmList.length > 0;
    }

    return {
        loadCC: loadCC,
        getWpm: getWpm,
        hasWpm: hasWpm
    };
};
