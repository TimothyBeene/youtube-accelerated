export const Config = {
    googleBaseApi: 'https://content.googleapis.com',
    googleApiKey: 'AIzaSyAvKP69emjYU1aXYxOXzN9EwL_c57eKZCw',
    smallIncriment: 0.1,
    largeIncriment: 0.2,
    threshold: 500,
    initialSpeed: 1.0,
    storageKey: 'youtube-accelerated-speed',
    defaultSettings: {
        "disable-memory": false,
        "small-increment": 0.1,
        "large-increment": 0.2,
        "disable-wpm-calc": true,
        "show-store-view": false
    }
}