// var fetch = require('node-fetch');
import { Config } from './config';

export function Category() {

    let catagories;
    let videoIdCatList = [];

    function getCatagories() {
        if (catagories) return catagories;
        const url = new URL(Config.googleBaseApi + '/youtube/v3/videoCategories');
        url.searchParams.append('part', 'snippet');
        url.searchParams.append('regionCode', 'US');
        url.searchParams.append('key', Config.googleApiKey);
        
        return fetch(url).then( res => res.json());
    }

    function storeCatagories(body) {
        catagories = body.items.map( item => ({
            id: item.id,
            title: item.snippet.title
        }));
        return catagories;
    }

    function getCatagory(videoId) {
        const url = new URL(Config.googleBaseApi + '/youtube/v3/videos');
        url.searchParams.append('id', videoId);
        url.searchParams.append('part', 'snippet');
        url.searchParams.append('key', Config.googleApiKey);
        return fetch(url).then( res => res.json() ).then(mapVideoCatagory);
    }
    function mapVideoCatagory(body) {
        return new Promise(resolve => resolve(body.items[0].snippet.categoryId));
    }

    return {
        getCatagories: getCatagories,
        storeCatagories: storeCatagories,
        getCatagory: getCatagory
    };
}

// category.getCatagories().then(res => res.json()).then( body => {
//     console.log(body.items.map(i => ({
//         // id: i.snippet.channelId,
//         // name: i.snippet.title,
//         etag: i.etag
//     })));
// });

// category.getCatagories()
//     .then(category.storeCatagories)
//     .then(console.log)

// category.getCatagory('Ks-_Mh1QhMc').then(console.log);