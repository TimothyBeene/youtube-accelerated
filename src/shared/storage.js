import { Observable } from 'rxjs';

import { Config } from './config';

export function GlobalSpeed() {

    let onChangeObserver;

    get().then( store => initSpeedInStorage(store) );

    function migrateStorage(store) {
        if (store && typeof store === 'number') {
            console.log('upgrading to new speed object', store);
            store = {
                'baseSpeed': +store,
                'catagories': []
            };
        }
        if (store && store.schemaVersion === undefined) {
            // upgrade to schema 2
            console.log('upgrading store schema from version 1 to version 2');
            store = {
                "schemaVersion": "2",
                "settings": {
                    ...Config.defaultSettings
                },
                "speedStore": {
                    'baseSpeed': store.baseSpeed,
                    'catagories': store.catagories
                }
            }
        }
        if (!store) {
            console.log('store not set initializing store using version 2');
            store = {
                "schemaVersion": "2",
                "settings": {
                    ...Config.defaultSettings
                },
                "speedStore": {
                    'baseSpeed': 1,
                    'catagories': []
                }
            }
        }
        return store;
    }
    function initSpeedInStorage(store) {
        return new Promise( resolve => {
            const migratedStore = migrateStorage(store);
            set( migratedStore ).then( _ => resolve(migratedStore) )
        });
    }

    function setSpeed(newValue, catagoryId) {
        return new Promise(resolve => {
            get().then(store => {
                const speedStore = store.speedStore;
                if (!catagoryId) {
                    set({
                        ...store,
                        'speedStore': {
                            ...speedStore,
                            'baseSpeed': newValue.toPrecision(2)
                        }
                    }).then(resolve);
                } else {
                    set({
                        ...store,
                        'speedStore': {
                            ...speedStore,
                            'catagories': {
                                ...speedStore.catagories,
                                [catagoryId]: newValue.toPrecision(2)
                            }
                        }
                    }).catch(console.log).then(resolve);
                }
            })
        });
    }

    function getSpeed(catagoryId) {
        return new Promise((resolve) => {
            get().then( store => {
                const speedStore = store.speedStore;
                if (!catagoryId) {
                    resolve(+speedStore.baseSpeed);
                } else {
                    if (speedStore.catagories[catagoryId]) {
                        resolve(+speedStore.catagories[catagoryId]);
                    } else {
                        resolve(+speedStore.baseSpeed);
                    }
                }
            })
        });
    }
    function get() {
        return new Promise((resolve) => {
            browser.storage.local.get(Config.storageKey).then(
                val => {
                    resolve(val[Config.storageKey]);
                }
            )
        });
    }
    function set(newValue) {
        return browser.storage.local.set({[Config.storageKey]: newValue});
    }
    
    function onChange() {
        return new Observable( observer => {
            onChangeObserver = observer;
            get().then( store => initSpeedInStorage(store) ).then( store => observer.next(store) );

            browser.storage.onChanged.addListener( (changes, areaName) => {
                if (areaName === 'local' && changes[Config.storageKey]) {
                    get().then( store => observer.next(store) );
                }
            });
        });
    }
    function forceEventPush() {
        get().then( speed => onChangeObserver.next(speed) );
    }

    return {
        setSpeed,
        getSpeed,
        onChange,
        forceEventPush,
        get,
        set
    };
}