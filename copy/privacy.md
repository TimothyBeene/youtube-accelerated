## Single Purpose Description

Adjust the speed of Youtube® videos via easy to UI directly on the control bar of the native Youtube® player.

## Storage Permission

To remember playback rate

## Host Permission

Access to "*://*.youtube.com/*" for adding the control UI to the Youtube® player.
Access to "https://content.googleapis.com/*" for getting meta data about current video.

## Remote Code

No
