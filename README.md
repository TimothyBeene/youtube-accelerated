


# Install

## Chrome
The listing for Youtube Accelerated was removed from the chrome webstore so for the time being you can use the following steps in stall the extension manually

* Download latest version from [releases](https://gitlab.com/TimothyBeene/youtube-accelerated/-/releases/) look for something like `video-accelerated-source-x.x.x.zip (external source)`
* Extract .zip file to a known location on your computer (i.e. your desktop)
* Open Chrome and navigate to `chrome://extensions/`
* Make sure `Developer mode` is set to on (located on the top right of the page)
* Click `Load unpacked` (located on the rop left of the page)
* Use the file navigator to select the location you extracted the extension to.

[Chrome store listing](https://chrome.google.com/u/1/webstore/devconsole/g03457414448479026831/jcielphejmlnnmibmmmibgjjgnbdjhhf/edit/listing?hl=en_US)

## Firefox
You can install on Firefox via the official site https://addons.mozilla.com.

[Firefox store listing](https://addons.mozilla.org/en-US/firefox/addon/youtube-accelerated/)

[Developer portal](https://addons.mozilla.org/en-US/developers/addon/youtube-accelerated/edit)
### Firefox Listing
Listing: https://addons.mozilla.org/en-US/firefox/addon/youtube-accelerated/
Link: 

#Icon Source
https://materialdesignicons.com/icon/play-speed

# TODO
https://gitlab.com/TimothyBeene/youtube-accelerated/-/boards