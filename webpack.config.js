const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin')

const distPath = path.resolve(__dirname, 'dist');

module.exports = {
  entry: {
    'main': './src/main.js',
    'options/options': './src/options/options.js'
  },
  output: {
    filename: '[name].js',
    path: distPath
  },
  mode: 'development',
  plugins: [
    new CopyWebpackPlugin([
      { from: 'src/manifest.json', to: 'manifest.json' },
      { from: 'src/assets/*', to: 'assets/', flatten: true },
      { from: 'src/css/*', to: 'css/', flatten: true },
      { from: 'src/lib/*', to: 'lib/', flatten: true },
      { from: 'src/options/*', to: 'options/', flatten: true }
    ], {})
  ]
};
